const express = require('express')
const router = express.Router()
const routerMaterias = require('./routes/materias')

router.use('/materias', routerMaterias)

module.exports = router
