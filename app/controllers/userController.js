const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

const index = (req, res) => {
  User.find((err, users) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el usuario'
      })
    }
    return res.json(users)
  })
}
const login = (req, res) => {
  User.findOne({ nombre: req.body.nombre }, function (err, user) {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el usuario'
      })
    }
    if (!user) {
      return res.status(404).json({
        message: 'No hemos encontrado el usuario'
      })
    }

    console.log(user)
    return res.status(200).send({ token: servicejwt.createToken(user) })
  })
}
module.exports = {
  index,
  login
}
