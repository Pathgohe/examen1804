const Materia = require('../models/Materia')

const index = (req, res) => {
  Materia.find((err, materia) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo la materia'
      })
    }
    return res.json(materia)
  })
}

const { ObjectId } = require('mongodb')
const show = (req, res) => {
  const id = req.params.id
  Materia.findOne({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no valido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener la materia'
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No tenemos esta materia'
      })
    }
    return res.json(materia)
  })
}

const create = (req, res) => {
  const materia = new Materia(req.body)
  materia.save((err, materia) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la materia',
        error: err
      })
    }
    return res.status(201).json(materia)
  })
}
const update = (req, res) => {
  const id = req.params.id
  Materia.findOne({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la materia',
        error: err
      })
    }

    Object.assign(materia, req.body)

    materia.save((err, materia) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la materia'
        })
      }
      if (!materia) {
        return res.status(404).json({
          message: 'No hemos encontrado la materia'
        })
      }
      return res.json(materia)
    })
  })
}

const remove = (req, res) => {
  const id = req.params.id

  Materia.findOneAndDelete({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado la materia'
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No hemos encontrado la materia'
      })
    }
    return res.json(materia)
  })
}

module.exports = {
  index,
  show,
  create,
  update,
  remove
}
