const express = require('express')
const router = express.Router()

// invocamos un nuevo controlador
const userController = require('../controllers/userController')

router.get('/', (req, res) => {
  console.log('rutas de usuarios')
  userController.index(req, res)
})
router.post('/login', (req, res) => {
  userController.login(req, res)
})

module.exports = router
