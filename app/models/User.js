const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  codigo: {
    type: Number,
    unique: true
  },
  name: String,
  password: { type: String, select: false }
})

module.exports = mongoose.model('User', UserSchema)
