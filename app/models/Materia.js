const mongoose = require('mongoose')

const materiaSchema = mongoose.Schema({
  codigo: {
    type: String,
    maxlength: 10,
    unique: true,
    required: true
  },
  nombre: {
    type: String,
    maxlength: 20,
    required: true
  },
  curso: {
    type: String,
    maxlength: 10
  },
  horas: {
    type: Number
  }
})

const Materia = mongoose.model('Materia', materiaSchema)

module.exports = Materia
